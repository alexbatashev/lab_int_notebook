var express = require('express');
var router = express.Router();
var db = require('../db');

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index');
});

router.get('/notes', function (req, res, next) {
    db.query('SELECT * FROM notes', function (err, data, f) {
        if (err) {
            console.log(err);
            res.status(500);
            res.end();
        } else {
            res.send(data);
        }
    })
});

router.post('/notes', function (req, res, next) {
    db.query('INSERT INTO notes (text) VALUES ("'+req.body.text+'")', function (err, data, f) {
        if (err) {
            console.log(err);
            res.status(500);
            res.end();
        } else {
            res.status(200);
            res.end()
        }
    })
});

router.delete('/notes/:id', function (req, res, next) {
    db.query('DELETE FROM notes WHERE id = ' + req.params.id, function (err, data, f) {
        if (err) {
            console.log(err);
            res.status(500);
            res.end();
        } else {
            res.status(200);
            res.end()
        }
    })
});

module.exports = router;
